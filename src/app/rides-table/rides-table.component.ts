import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { RideDetailComponent } from '../ride-detail/ride-detail.component';
import { RideService } from '../rides.service';

export interface Ride {
  id: number;
  distance: number;
  startTime: string;
  duration: number;
}

@Component({
  selector: 'app-rides-table',
  templateUrl: './rides-table.component.html',
  styleUrls: ['./rides-table.component.css']
})
export class RidesTableComponent implements OnInit {

  displayedColumns: string[] = ['id', 'distance', 'startTime', 'duration', 'action'];
  dataSource: any = new MatTableDataSource<Ride>();
  loading: boolean;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(private rideService: RideService, public dialog: MatDialog) {
    this.getAllRides();
  }

  getAllRides() {
    this.loading = true;
    this.rideService.getAllRides().subscribe(data => {
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.loading = false;
    });
  }

  openDialog(ride: Ride): void {
    this.rideService.calculateFare(ride).subscribe(data => {
      const dialogRef = this.dialog.open(RideDetailComponent, {
        width: '400px',
        data: data
      });
      dialogRef.afterClosed().subscribe(result => { });
    });
  }

  createRide(): void {
    this.loading = true;
    this.rideService.createRide().subscribe(data => {
      this.dataSource.data.push(data);
      this.paginator._changePageSize(this.paginator.pageSize);
      this.paginator.length = this.dataSource.data.length;
      this.loading = false;
    });
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {

  }

}
