import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';
import { Ride } from './rides-table/rides-table.component';

@Injectable({
  providedIn: 'root'
})
export class RideService {

  server: string = environment.SERVER_API;
  ridesEndpoint: string = environment.GET_RIDES_ENDPOINT;
  createRideEndpoint: string = environment.START_RIDES_ENDPOINT;
  calculateFareEndpoint: string = environment.CALCULATE_FARE_ENDPOINT;

  constructor(private http: HttpClient) { }

  getAllRides(): Observable<any[]> {
    return this.http.get<any[]>(`${this.server}${this.ridesEndpoint}`);
  }

  createRide(): Observable<any> {
    return this.http.post(`${this.server}${this.createRideEndpoint}`, undefined);
  }

  calculateFare(ride: Ride): Observable<any> {
    return this.http.post<Ride>(`${this.server}${this.calculateFareEndpoint}`, ride);
  }

}
