export const environment = {
  production: true,
  SERVER_API: 'https://taxi-ride-calculator-service-qkj6kidhzq-uk.a.run.app/v1',
  GET_RIDES_ENDPOINT: '/rides',
  START_RIDES_ENDPOINT: '/rides/start',
  CALCULATE_FARE_ENDPOINT: '/rides/fare/calculate',
};
